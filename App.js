import React from 'react';
import * as firebase from 'firebase/app';
import { Text, Animated, Easing, Linking } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import MainStack from './src/Main/MainStack';
import Login from './src/Auth/login';
import reducers from "./src/reducers/storeReducer";
import Thunk from "redux-thunk";
import LottieView from "lottie-react-native";
import Loading from "./assets/gears.json";
import { Permissions,  WebBrowser } from "expo";
// import { composeWithDevTools  } from "redux-devtools-extension";


const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(), applyMiddleware(Thunk));
export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fontLoaded: false,
      loggedIn: null,
      progress: new Animated.Value(0),
      errorMessage: '',
      redirectData: null
    };
  }

  async componentWillMount() {
    try {
      await Expo.Font.loadAsync({
        'Roboto': require('native-base/Fonts/Roboto.ttf'),
        'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      });
      this.setState({ fontLoaded: true });
    } catch (error) {
      this.setState({ fontLoaded: false });
    }

    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true });
      }
      else {
        this.setState({ loggedIn: false });
      }
    });

    this.getCurrentLocation();
  }

  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 5000,
      easing: Easing.linear,
    }).start();
  }

  renderIntialView() {
    switch (this.state.loggedIn) {
      case true:
        return <viewMerchant />
      default:
        return <Login />
    }
  }

  getCurrentLocation = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      Linking.openURL('app-settings: ')
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }
  }

  render() {
    return (

      <Provider store={store}>
        {this.state.fontLoaded ?
          <MainStack />
          :
          <LottieView source={Loading} progress={this.state.progress} style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5, flex: 2, }} />}
      </Provider>
    );
  }
}


