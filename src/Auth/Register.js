import React from "react";
import { Container, Text, Header, Body, Left, Button, Icon, Content, Item, Input, Label, Right, Title } from "native-base";
import { View, TextInput, ImageBackground, StyleSheet, TouchableOpacity, Dimensions, AsyncStorage } from "react-native";
import * as firebase from 'firebase';
import ShakingText from 'react-native-shaking-text';
import * as Animatable from 'react-native-animatable';

const { width: WIDTH } = Dimensions.get('window')

export default class Login extends React.Component {
    state = {
        email: '',
        password: '',
        error: '',
        loading: false,
        showPass: true,
        press: false
    }


    //  Component is used to register with firebase
    register() {
        this.setState({ error: '', loading: true });
        const { email, password } = this.state;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((r) => {
                console.log(r)
                this.setState({ error: '', loading: false });
                //used if the user successfully creates a new account, the user will be directed to the auth register
                this.props.navigation.navigate('RegisterAuth');
            })
            // used to find out the type of error from firebase,
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;

                // error if the email has already been used
                if (errorCode == 'auth/email-already-in-use') {
                    this.setState({ error: 'Account is already used' })
                }
                //error if the format email invalid
                else if (errorCode == 'auth/invalid-email') {
                    this.setState({ error: 'Email format invalid' })
                }
                //error if password not strong
                else if (errorCode == 'auth/weak-password') {
                    this.setState({ error: 'Your password is not strong enough' })
                }
                //error default
                else {
                    this.setState({
                        error: 'Register Failed'
                    })
                }

            });
    }

    onAuthSuccess() {
        this.setState({
            email: '',
            password: '',
            error: '',
            loading: false,

        })
    }

    // function to see the password or not
    showPass = () => {
        if (this.state.press == false) {
            this.setState({ showPass: false, press: true })
        }
        else {
            this.setState({ showPass: true, press: false })
        }
    }

    render() {
        return (
            <Container>

                {/* Component for header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Register Seller</Title>
                    </Body>
                    <Right></Right>
                </Header>

                {/* Component for adding value */}
                <Content style={{ marginTop: 15, paddingLeft: 15, paddingRight: 15 }}>
                    <Animatable.View animation="flipInX" duration={1200}>
                        <Item rounded>
                            <Input placeholder="Email" keyboardType="email-address" autoCapitalize="none"
                                text={this.state.email}
                                onChangeText={email => this.setState({ email })} />
                        </Item>
                        <Item rounded style={{ marginTop: 20 }}>
                            <Input
                                placeholder="Password"
                                autoCapitalize="none"
                                secureTextEntry={this.state.showPass}
                                text={this.state.password}
                                onChangeText={password => this.setState({ password })} />
                            <TouchableOpacity style={styles.btn_eye} onPress={this.showPass.bind(this)}>
                                <Icon name={this.state.press == false ? 'ios-eye' : 'ios-eye-off'} size={28} color={'rgba(255,255,255,0.7)'} />
                            </TouchableOpacity>
                        </Item>
                        {this.state.error != '' ?
                            <ShakingText style={styles.errorMessage}>
                                {this.state.error}
                            </ShakingText> :
                            null
                        }

                        {/* Component Button for register user with value in text input */}
                        <Button rounded block onPress={() => this.register()} style={{ marginTop: 20 }}>
                            <Text>Register</Text>
                        </Button>
                    </Animatable.View>
                </Content>
            </Container>

        );
    }
}

const styles = StyleSheet.create({
    input: {
        marginLeft: 15,
        marginRight: 15,
    },
    inputType: {
        fontStyle: 'normal',
        fontSize: 15,
    },
    btn_login: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#E81636',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
    },
    login: {
        fontSize: 16,
        color: 'rgba(255,255,255,0.7)',
        textAlign: 'center'
    },
    errorMessage: {
        marginTop: 15,
        fontSize: 15,
        color: 'red',
        alignSelf: 'center'
    },
    btn_eye: {
        position: 'absolute',
        top: 14,
        right: 27,

    },
})

