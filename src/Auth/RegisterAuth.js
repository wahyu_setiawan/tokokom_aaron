import React from "react";
import { StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { View, Item, Form, Input, Content, Container, Text, Body, Header, Left, Button, Icon } from "native-base";

const { width: WIDTH } = Dimensions.get('window');

export default class SuccessReset extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (

            // This page is used to validate if you don't have a store, after that you will be transferred to the merchant add to add a store
            <View style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}>
                <Text>You don't have a shop yet, let's make it</Text>
                <Button onPress={() => this.props.navigation.navigate('AddMerchant')}
                    block warning style={{ borderRadius: 10, marginLeft: 15, marginRight: 15 }}>
                    <Icon name="ios-home" />
                    <Text>Add My Store</Text>
                </Button>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    successReset: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        flex: 2,
    }
})
