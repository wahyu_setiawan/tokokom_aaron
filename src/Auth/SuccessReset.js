import React from "react";
import { StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { View, Item, Form, Input, Content, Container, Text, Body, Header, Left, Button, Icon } from "native-base";

const { width: WIDTH } = Dimensions.get('window');

export default class SuccessReset extends React.Component {
    constructor(props) {
        super(props)
    }

    backToHome() {
        this.props.navigation.navigate('Main');
    }
    render() {
        return (
            <View style={styles.successReset}>
                <Icon name="ios-checkmark-circle-outline" />
                <Text>
                    Reset password link has been sent to your email
                </Text>
                <Button onPress={() => this.backToHome()}
                    bordered outlined style={{ alignSelf:'center', borderRadius: 10, marginLeft: 15, marginRight: 15, marginTop: 20, }}>
                    <Icon name="ios-home" />
                    <Text>Go To Home</Text>
                </Button>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    successReset: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        flex: 2,
    }
})
