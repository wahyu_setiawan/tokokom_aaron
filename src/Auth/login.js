import React from "react";
import { Container, Text, Header, Body, Left, Button, Icon, Content, Item, Input, Right, Title } from "native-base";
import { View, StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import * as firebase from "firebase";
import ShakingText from 'react-native-shaking-text';
import * as Animatable from 'react-native-animatable';


const { width: WIDTH } = Dimensions.get('window')


export default class Login extends React.Component {
    state = {
        email: '',
        password: '',
        error: '',
        loading: false,
        showPass: true,
        press: false
    }

    // function used to log in using e-mail and password from firebase
    LoginPress() {
        this.setState({
            error: '', loading: true
        });
        const { email, password } = this.state;
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                this.setState({ error: '', loading: false });
                this.props.navigation.navigate('Dashboard');
            })
            .catch(this.onAuthFailed.bind(this));
    }

    onAuthSuccess() {
        this.setState({
            email: '',
            password: '',
            error: '',
            loading: false
        })
    }
    onAuthFailed() {
        this.setState({
            error: 'Authentication Failed'
        })
    }

    // the function used to see the password or not
    showPass = () => {
        if (this.state.press == false) {
            this.setState({ showPass: false, press: true })
        }
        else {
            this.setState({ showPass: true, press: false })
        }
    }

    render() {
        return (
            <Container>

                {/* Component for header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Login Seller</Title>
                    </Body>
                    <Right />
                </Header>

                {/* Component for input data  */}
                <Content>
                    <Animatable.View animation="flipInX" duration={1200}>
                        <Item rounded style={styles.input}>
                            <Input style={styles.inputType} keyboardType="email-address" placeholder='Email' autoCapitalize='none'
                                onChangeText={email => this.setState({ email })} />
                        </Item>
                        <Item rounded warning style={styles.input}>
                            <Input style={styles.inputType} placeholder='Password' autoCapitalize='none'
                                onChangeText={password => this.setState({ password })} secureTextEntry={this.state.showPass} />
                            <TouchableOpacity style={styles.btn_eye} onPress={this.showPass.bind(this)}>
                                <Icon name={this.state.press == false ? 'ios-eye' : 'ios-eye-off'} size={28} color={'rgba(255,255,255,0.7)'} />
                            </TouchableOpacity>
                        </Item>
                        {
                            this.state.error != '' ?
                                <ShakingText style={styles.errorMessage}>
                                    {this.state.error}
                                </ShakingText>
                                :
                                null
                        }

                        {/* Button to login using firebase validation */}
                        <Button rounded block onPress={() => this.LoginPress()} style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                            <Text>Login</Text>
                        </Button>
                        {/* Button that is used for users who forget their password, then you just enter your email and you will get an email from firebase */}
                        <Button block bordered rounded onPress={() => this.props.navigation.navigate('ResetPassword')} style={{ marginLeft: 15, marginRight: 15, marginTop: 20 }}>
                            <Text>Forget Password</Text>
                        </Button>

                        {/* Register  */}
                        <Text style={{ fontWeight: 'bold', fontSize: 16, alignSelf: 'center', marginTop: 20, marginBottom: 20 }}>Haven't registered? Go to register.</Text>


                        {/* component is used to move pages */}
                        <Button rounded bordered block onPress={() => this.props.navigation.navigate('Register')} style={{ marginLeft: 15, marginRight: 15 }}>
                            <Text>Register</Text>
                        </Button>
                        {/* <Button rounded bordered block onPress={() => this.props.navigation.navigate('ViewMerchant')} style={{ marginTop: 10, marginLeft: 15, marginRight: 15 }}>
                            <Text>Merchant</Text>
                        </Button> */}
                    </Animatable.View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        marginTop: 19,
        marginLeft: 15,
        marginRight: 15,
    },
    inputType: {
        fontStyle: 'normal',
        fontSize: 15,
        paddingLeft: 14,
    },
    btn_login: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#FFA43F',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        shadowColor: 'rgba(232, 34, 32, 2)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: {
            width: 1,
            height: 13
        },
    },
    login: {
        fontSize: 16,
        color: 'rgba(255,255,255,0.7)',
        textAlign: 'center'
    },
    btn_register: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#E81636',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        shadowColor: 'rgba(232, 34, 32, 2)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: {
            width: 1,
            height: 13
        },
    },
    errorMessage: {
        marginTop: 15,
        fontSize: 15,
        color: 'red',
        alignSelf: 'center'
    },
    btn_eye: {
        position: 'absolute',
        top: 14,
        right: 27,

    },
    forgetPassword: {
        fontSize: 10,
    }
})

