import { createStackNavigator } from "react-navigation";
import ViewMerchant from "./viewMerchant";
import AddMerchant from "./addMerchant";
import Setting from "../Dashboard/Setting/SettingStack";
import ProductDetail from "../Dashboard/Product/ProductDetail";
import AddProduct from "../Dashboard/Product/addProduct";
import AddingMap from "../Dashboard/addingMap";
import ProductList from "../Dashboard/Product/productList";

const Dashboard = createStackNavigator({
    ViewMerchant: {
        screen: ViewMerchant,
        navigationOptions: {
            header: null
        }
    },
    AddMerchant: {
        screen: AddMerchant,
        navigationOptions: {
            header: null
        }
    },
    Setting: {
        screen: Setting,
        navigationOptions: {
            header: null
        }
    },
    AddProduct: {
        screen: AddProduct,
        navigationOptions: {
            header: null
        }
    },
    AddingMap: {
        screen: AddingMap,
        navigationOptions: {
            header: null
        }
    },
    ProductList: {
        screen: ProductList,
        navigationOptions: {
            header: null
        }
    },
    ProductDetail: {
        screen: ProductDetail,
        navigationOptions: {
            header: null
        }
    }

})

export default Dashboard;