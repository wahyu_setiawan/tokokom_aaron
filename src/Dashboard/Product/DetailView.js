import React from "react";
import {
    Text, Container, Header, Left,
    Button, Body, Icon, Content, Card, CardItem, View, Right, Title, Separator
} from "native-base";
import { Animated, Easing, ScrollView, Image, Dimensions, Linking } from "react-native";
import openMap from 'react-native-open-maps';
import Icon2 from "react-native-vector-icons/Entypo";
import { Collapse, CollapseHeader, CollapseBody } from "accordion-collapse-react-native";
import noImage from "../../../assets/noImage.png";
import * as  Animatable from "react-native-animatable";
// import Accordion from "react-native-accordion";

class DetailView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaBarang: '',
            hargaBarang: '',
            beratBarang: '',
            listBarang: [],
            isLoading: true,
            error: '',
            isLoadingBarang: true,
            progress: new Animated.Value(0)
        }
    }

    componentWillMount() {
        //-----------> get item data from the main page by adding {data} at this.props.navigation.navigate, then each key will be checked, this item has a shop <-------------
        let barang = this.props.navigation.state.params.data.child('barang').val()
        if (barang) {
            Object.keys(barang).forEach((key) => {
                // add items from each key to state.list Item
                this.state.listBarang.push(barang[key])
            });
        }
    }


    //  Component for animation
    componentDidMount() {
        Animated.timing(this.state.progress, {
            toValue: 1,
            duration: 1000,
            easing: Easing.linear,
        }).start();
    }


    // Function for open up google maps 
    goToMaps() {
        openMap({
            latitude: this.props.navigation.state.params.data.child('toko/latitude').val(),
            longitude: this.props.navigation.state.params.data.child('toko/longitude').val(),
            query: this.props.navigation.state.params.data.child('toko/namaToko').val()
        });
    }

    render() {
        return (
            <Container>



                {/* Component Header */}
                <Header style={{ paddingTop: 25, height: 80, }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{this.props.navigation.state.params.data.child('toko/namaToko').val()}</Title>
                    </Body>
                    <Right />
                </Header>


                {/* Component identity store from firebase */}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{ padding: 20, }}>
                        <Icon name="md-business" fontSize={34} />
                        <Text>Store Name</Text>
                        <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                            {this.props.navigation.state.params.data.child('toko/namaToko').val()}
                        </Text>
                        <Icon2 name='address' style={{ fontSize: 20 }} />
                        <Text>Address</Text>
                        <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                            {this.props.navigation.state.params.data.child('toko/alamat').val()}
                        </Text>
                        <Icon name='md-list' style={{ fontSize: 20 }} />
                        <Text>Description</Text>
                        <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                            {this.props.navigation.state.params.data.child('toko/describe').val()}
                        </Text>
                    </View>

                    {/* Component for open call and location */}
                    <View style={{ flexDirection: 'row', alignSelf: 'center', marginLeft: 20, marginRight: 20 }}>
                        <Button bordered success onPress={() => Linking.openURL('tel:' + this.props.navigation.state.params.data.child('toko/phone').val())}>
                            <Icon name="md-call" />
                            <Text>Call Store</Text>
                        </Button>
                        <Button bordered style={{ marginLeft: 10 }} onPress={() => this.goToMaps()}>
                            <Icon name="md-locate" />
                            <Text>Get Location</Text>
                        </Button>
                    </View>



                    {/* Component for product list and data we get from main page  */}
                    <View style={{ marginTop: 20, marginBottom: 10, alignSelf: 'center' }}>
                        <Text style={{ fontWeight: '700' }}>Product List</Text>
                    </View>
                    {
                        this.state.listBarang.length == 0 ?
                            <View style={{ alignSelf: 'center', marginTop: 10, flex: 1 }}>
                                <Text style={{ fontWeight: '700' }}>
                                    No products available for this store
                                     </Text>
                            </View>
                            : (

                                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                    {this.state.listBarang.map((data, i) => {
                                        return (
                                            <Animatable.View key={i} animation="zoomIn" duration={1200}>
                                                <Content >
                                                    <Card style={{ marginBottom: 10, borderRadius: 15, marginLeft: 10, marginRight: 10, }}>
                                                        {
                                                            data.gambar != '' ?
                                                                <CardItem header>
                                                                    <Image style={{
                                                                        resizeMode: 'cover',
                                                                        width: Dimensions.get('window').width,
                                                                        height: 250
                                                                    }} source={{ uri: data.gambar }} />
                                                                </CardItem>
                                                                :
                                                                <Image source={noImage} style={{
                                                                    resizeMode: 'cover',
                                                                    width: Dimensions.get('window').width,
                                                                    height: 275
                                                                }} />
                                                        }
                                                        <CardItem Body>
                                                            <Text>{data.namaBarang}</Text>
                                                        </CardItem>
                                                        <CardItem footer>
                                                            <Text> Rp. {data.hargaBarang}</Text>
                                                        </CardItem>
                                                    </Card>
                                                </Content>
                                            </Animatable.View>
                                        )
                                    })
                                    }
                                </ScrollView>

                            )
                    }
                </ScrollView>

            </Container>
        )
    }
}


export default DetailView;