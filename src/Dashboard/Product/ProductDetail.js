import React from 'react';
import { StyleSheet, ScrollView, Dimensions, Image, Animated } from 'react-native';
import { connect } from 'react-redux';
import { Text, Container, Left, Body, Header, Icon, Button, Title, Right } from "native-base";
import _ from 'lodash';
import * as firebase from "firebase";
import * as actions from "../../actions";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import { ConfirmDialog } from "react-native-simple-dialogs";
import NoImage from "../../../assets/noImage.png"
// import Accordion from "react-native-accordion";
// import * as actions from "../../actions";

const { width: WIDTH } = Dimensions.get('window')

class ProductDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaBarang: '',
            hargaBarang: '',
            beratBarang: '',
            listBarang: [],
            isLoading: true,
            error: '',
            isLoadingBarang: true,
            progress: new Animated.Value(0)
        }
    }


    //----------------> Function for get validation delete product or not <-------------------------
    openConfirm = (show) => {
        this.setState({ showConfirm: show });
    }

    // -------------> if we choose yes product will delete  permanently <--------------------
    optionYes = async () => {
        this.openConfirm(false);
        try {
            await this.deleteProduct();
        } catch (e) {
            // console.log(e);
        }
        setTimeout(() => {
            alert('Item has been permanently deleted')
        }, 300);

        this.props.navigation.navigate('ProductList');
    }

    // -------------------> If we choose no we will move to current page <-------------------------
    optionNo = () => {
        this.openConfirm(false);
        setTimeout(() => {
            this.props.navigation.navigate('ProductDetail')
        }, 300);
    }


    //-------------> Function for deleting product by key based on the key that we get from the product list by carrying data using navigation <---------------
    deleteProduct() {
        this.props.deleteProduct(this.props.navigation.state.params.data.key)
    }

    render() {
        return (
            <Container>

                {/* Component Header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Product Detail</Title>
                    </Body>
                    <Right></Right>
                </Header>

                {/* Component for value from bringin data navigation*/}
                <ScrollView style={{ margin: 15 }} showsVerticalScrollIndicator={false}>


                    {/* Getting image from list product by bringing data from navigation,  */}
                    {
                        this.props.navigation.state.params.data.child('gambar').val() != '' ?
                            <Image style={style.image_preview} source={{ uri: this.props.navigation.state.params.data.child('gambar').val() }} />
                            :
                            <Image style={style.image_preview} source={NoImage} />
                    }
                    <Text>Product Name</Text>
                    <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                        {this.props.navigation.state.params.data.child('namaBarang').val()}
                    </Text>
                    <Text>Product Weight</Text>
                    <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                        {this.props.navigation.state.params.data.child('beratBarang').val()} kg
                    </Text>
                    <Text>Price</Text>
                    <Text style={{ fontWeight: '700', marginBottom: 20 }}>
                        Rp {this.props.navigation.state.params.data.child('hargaBarang').val()}
                    </Text>


                    {/* Component button for get validation want to delete or not */}
                    <Button rounded block danger onPress={() => this.openConfirm(true)}>
                        <Text>Delete Product</Text>
                    </Button>
                    <ConfirmDialog
                        title="Confirm Dialog"
                        message="Are you sure delete this product?"
                        onTouchOutside={() => this.openConfirm(false)}
                        visible={this.state.showConfirm}
                        negativeButton={
                            {
                                title: "NO",
                                onPress: this.optionNo,
                                titleStyle: {
                                    color: "blue",
                                    colorDisabled: "aqua",
                                },
                                style: {
                                    backgroundColor: "transparent",
                                    backgroundColorDisabled: "transparent",
                                },
                            }
                        }
                        positiveButton={
                            {
                                title: "YES",
                                onPress: this.optionYes,
                            }
                        }
                    />
                </ScrollView>
            </Container>
        )
    }
}

const style = StyleSheet.create({
    image_preview: {
        marginTop: 0,
        alignSelf: 'center',
        marginBottom: 5,
        width: Dimensions.get('window').width,
        height: 300,
        marginBottom: 10
    },
    btn_delete: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#E81636',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        shadowColor: 'rgba(232, 34, 32, 2)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: {
            width: 1,
            height: 13
        },
    },
})


export default connect(null, actions)(ProductDetail);
