import React from "react";
import { Container, Header, Left, Button, Icon, Body, Content, Item, Label, Input, Text, View, Title, Right, Spinner } from "native-base";
import { StyleSheet, Image } from "react-native";
import * as actions from "../../actions";
import { connect } from "react-redux";
import getPermissions from "../../getPermissions";
import { ImagePicker, Permissions, } from "expo";
import * as firebase from "firebase";

const options = {
    allowsEditing: true,
};

class addProduct extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            namaBarang: '',
            hargaBarang: '',
            beratBarang: '',
            gambar: '',
            isLoading: true,
            progress: 0,
            image_preview: null,
            error: '',
            isPictureUpload: false
        }
    }


    //---------------> Function for adding barang and validation if text input still empty error will appear <----------------------------
    addBarang() {
        if (this.props.namaBarang == '' || this.props.hargaBarang == '') {
            this.setState({ error: 'Product name and price must be filled' })
        } else {
            this.setState({ error: '' })
            const { namaBarang, hargaBarang, beratBarang, } = this.props;
            this.props.createNewBarang({ namaBarang, hargaBarang, beratBarang, gambar: this.state.gambar });
            this.props.navigation.navigate('ProductList')
        }

    }

    // --------------------------> Function for getting permissions from phone for access to gallery and then upload to the app first before uploading to firebase <----------------
    _openPicker = async () => {
        const status = await getPermissions(Permissions.CAMERA_ROLL);
        if (status) {
            let result = await ImagePicker.launchImageLibraryAsync(options);
            let name = new Date().getTime() + '-media.jpg'
            if (!result.cancelled) {
                await this.uploadImage(result.uri, name)
                    .then(() => {
                        alert('Success Uploaded' + this.state.gambar);
                        this.setState({ isPictureUpload: false })
                    })
                    .catch((error) => {
                        alert(error);
                    })
            }
        }
    }

    // --------------> Same function as above but this but this function is uploaded to firebase and displays to the app <-----------------
    uploadImage = async (uri, imageName) => {
        this.setState({ isPictureUpload: true })
        const response = await fetch(uri);
        const blob = await response.blob();

        var Imageref = firebase.storage().ref("/images/" + imageName);
        const snapshot = await Imageref.put(blob)

        // Listen for state changes, errors, and completion of the upload.
        // Upload completed successfully, now we can get the download URL
        snapshot.ref.getDownloadURL().then(downloadURL => this.setState({
            gambar: downloadURL
        }))
    }

    render() {
        return (
            <Container>

                {/* Component Header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Add Product</Title>
                    </Body>
                    <Right></Right>
                </Header>
                <Content>
                    {
                        this.state.gambar != '' ?
                            <Image style={styles.image_preview} source={{ uri: this.state.gambar }} />
                            : null
                    }
                    {
                        this.state.isPictureUpload ?
                            <Spinner color="#AAAAAA" />
                            :
                            null
                    }
                    <Item style={styles.input}>
                        <Input value={this.props.namaBarang} placeholder="Product Name"
                            onChangeText={value => this.props.formUpdateBarang({ prop: 'namaBarang', value })} />
                    </Item>
                    <Item style={styles.input}>
                        <Input value={this.props.hargaBarang} keyboardType='phone-pad' placeholder="Price (IDR)"
                            onChangeText={value => this.props.formUpdateBarang({ prop: 'hargaBarang', value })} />
                    </Item>
                    <Item style={styles.input}>
                        <Input value={this.props.beratBarang} keyboardType='phone-pad' placeholder="Weight (kg)"
                            onChangeText={value => this.props.formUpdateBarang({ prop: 'beratBarang', value })} />
                    </Item>
                    {
                        this.state.error != '' ?
                            <Text style={{ marginLeft: 15, marginTop: 20, color: 'red' }}>{this.state.error}</Text>
                            :
                            null
                    }


                    {/* Component for upload image and get image from gallery */}
                    <Button disabled={this.state.isPictureUpload} rounded bordered block style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }} onPress={() => this._openPicker()}>
                        <Icon name="md-cloud-upload" />
                        <Text>Upload Image</Text>
                    </Button>

                    {/* Component for adding all value to firebase */}
                    <Button disabled={this.state.isPictureUpload} rounded block onPress={() => this.addBarang()} style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                        <Icon name="md-add" />
                        <Text>Add Product</Text>
                    </Button>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    input: {
        marginTop: 19,
        marginLeft: 15,
        marginRight: 15,
    },
    button: {
        flexDirection: 'row',
        marginTop: 20,
        alignSelf: 'center'
    },
    btn_add: {
        marginRight: 20,
        backgroundColor: '#5CA2FF'
    },
    btn_reset: {
        marginLeft: 20,
        backgroundColor: '#FF0000'
    },
    image_preview: {
        flexDirection: 'row',
        marginTop: 10,
        alignSelf: 'center',
        marginBottom: 5,
        width: 90,
        height: 90,
    }
})

const mapStateToProps = state => {
    const { namaBarang, hargaBarang, beratBarang, } = state;
    return { namaBarang, hargaBarang, beratBarang, };
};

export default connect(mapStateToProps, actions)(addProduct);