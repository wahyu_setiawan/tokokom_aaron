import React, { Component } from 'react';
import { StyleSheet, ScrollView, TouchableOpacity, Dimensions, Image, RefreshControl, Animated, Easing, ListView, View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Container, Content, Card, CardItem, Left, Body, Header, Icon, Button } from "native-base";
import _ from 'lodash';
import * as actions from "../../actions";
// import * as firebase from "firebase";
// import ProductItem from "./productItem";

const ProductItem = (props) => {
    return (
        <Content>
            <Card style={{ marginBottom: 20 }}>
                <CardItem header style={{ backgroundColor: '#FFA843' }}>
                    <Text>{props.barangku.namaBarang}</Text>
                </CardItem>
                <CardItem cardBody onPress={() => props.selectBarang(barangku)}>
                    <Image style={{
                        resizeMode: 'cover',
                        width: Dimensions.get('window').width,
                        height: 250
                    }} source={{ uri: props.barangku.gambar }} />
                </CardItem>
                <CardItem footer style={{ backgroundColor: '#FFA843', justifyContent: 'center' }}>
                    <Text> Rp. {props.barangku.hargaBarang}</Text>
                </CardItem>
            </Card>
        </Content>
    )
}

export default connect(null, actions) (ProductItem);