import React from 'react';
import { StyleSheet, ScrollView, Dimensions, Image, Animated, View, TouchableOpacity } from 'react-native';
import { Text, Container, Content, Card, CardItem, Left, Body, Header, Icon, Button, Right, Title, Fab } from "native-base";
import _ from 'lodash';
import * as firebase from "firebase";
import Ripple from "react-native-material-ripple";
import * as  Animatable from "react-native-animatable";
import noImage from "../../../assets/noImage.png";

const { width: WIDTH } = Dimensions.get('window')

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 353,
        flexWrap: 'wrap',
        paddingTop: 20,
        paddingLeft: 20,
    },
    btn_add: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 15,
        backgroundColor: '#FFA843',
        justifyContent: 'center',
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 20,
        shadowColor: 'rgba(232, 34, 32, 2)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: {
            width: 1,
            height: 13
        },
    },
    login: {
        fontSize: 16,
        color: 'rgba(255,255,255,0.7)',
        textAlign: 'center'
    }
});

class productList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            listBarangku: [],
            isLoading: true
        }
    }


    //------------------> component that must be run first with a function that must be run <-------------------
    componentWillMount() {
        this.getBarang();
    }


    //---------------------------> Function for get barang from uid current user, and change to response and changed to json file <----------------------
    getBarang() {
        const { currentUser } = firebase.auth()
        var uid = currentUser.uid;
        var rootRef = firebase.database().ref('/users/' + uid + '/barang');
        rootRef.on("value", (response) => {
            this.state.listBarangku = [];
            response.forEach((data) => {
                this.state.listBarangku.push(data)
            })
            this.setState({ isLoading: false })
            console.log(this.state)
        })

    }

    render() {
        return (
            <Container>


                {/* Component Header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left >
                    <Body>
                        <Title>Products List</Title>
                    </Body>
                    <Right></Right>
                </Header>


                {/* Button for adding product, In this case we will direct to addProduct page */}
                <Button icon rounded onPress={() => this.props.navigation.navigate('AddProduct')} style={{ position: 'absolute', bottom: 15, right: 15, width: 50, height: 50, zIndex: 99 }}>
                    <Icon name="md-add" style={{ position: 'relative', left: 2 }} />
                </Button>


                {/*  If the item is there, it will appear if not, then the text No product is available  */}
                <ScrollView style={{ padding: 10 }}>
                    {
                        this.state.listBarangku.length > 0 ?
                            (

                                //-----------> use of data in lieu of this.state. my list. and I as the key for repetition <--------- 
                                this.state.listBarangku.map((data, i) => {
                                    return (
                                        <Animatable.View key={i} animation="zoomIn" duration={700}>


                                            {/*  The use of throwing data from the page in to another page by adding {data} behind to bring the data to the destination page*/}
                                            <Ripple onPress={() => this.props.navigation.navigate('ProductDetail', { data })}>
                                                <Card style={{ marginBottom: 10, borderRadius: 15, }}>
                                                    {
                                                        data.child('gambar').val() != '' ?
                                                            <CardItem cardBody>
                                                                <Image style={{
                                                                    resizeMode: 'cover',
                                                                    width: Dimensions.get('window').width,
                                                                    height: 250
                                                                }} source={{ uri: data.child('gambar').val() }} />
                                                            </CardItem>
                                                            :
                                                            <Image source={noImage} style={{
                                                                resizeMode: 'cover',
                                                                width: Dimensions.get('window').width,
                                                                height: 275
                                                            }} />
                                                    }
                                                    <CardItem header>
                                                        <Text>{data.child('namaBarang').val()}</Text>
                                                    </CardItem>
                                                    <CardItem footer>
                                                        <Text> Rp. {data.child('hargaBarang').val()}</Text>
                                                    </CardItem>
                                                </Card>
                                            </Ripple>
                                        </Animatable.View>
                                    )
                                })
                            )
                            :
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                <Icon name="md-sad" style={{ color: '#CCCCCC' }} />
                                <Text style={{ color: '#CCCCCC' }}>No Products Available</Text>
                            </View>
                    }
                </ScrollView>
            </Container>
        )
    }
}

export default (productList);
