import React from "react";
import { StyleSheet, TouchableOpacity, Dimensions } from "react-native";
import { View, Item, Form, Input, Content, Container, Text, Body, Header, Left, Button, Icon, Right, Title, Spinner } from "native-base";
import * as firebase from "firebase";

// const { width: WIDTH } = Dimensions.get('window')

const { width: WIDTH } = Dimensions.get('window');


export default class Password extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            error: '',
            isLoading: false
        }
    }

    resetPassword() {
        this.setState({ isLoading: true })
        if (this.state.email != '') {
            var actionCodeSettings = {
                url: 'https://tokokom.page.link/fpwd',
                iOS: {
                    bundleId: 'com.fiture.tokokom'
                },
                android: {
                    packageName: 'com.fiture.tokokom',
                    installApp: false,
                    minimumVersion: '12'
                },
                handleCodeInApp: true,
                dynamicLinkDomain: 'https://tokokom.page.link'
            };
            const { email } = this.state;
            firebase.auth().sendPasswordResetEmail(
                email, actionCodeSettings)
                .then((data) => {
                    this.setState({ error: '', isLoading: false })
                    // Password reset email sent.
                    this.props.navigation.navigate('SuccessReset')
                })
                .catch((error) => {
                    console.log(error);
                    this.setState({
                        error: 'Email is invalid',
                        isLoading: false
                    })
                })

        }
        else {
            this.setState({ error: 'Email must be filled', isLoading: false })
        }
    }

    render() {
        return (
            <Container>
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Reset Password</Title>
                    </Body>
                    <Right></Right>
                </Header>
                <Content>
                    <Item rounded style={styles.input}>
                        <Input style={styles.inputType} placeholder='Email Address' autoCapitalize='none'
                            onChangeText={email => this.setState({ email })} />
                    </Item>
                    {
                        this.state.error != '' ?
                            <Text style={styles.errorMessage}>
                                {this.state.error}
                            </Text> : null
                    }

                    {
                        this.state.isLoading ?
                            <Button rounded block disabled style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                                <Spinner color="#FFFFFF"/>
                            </Button> :
                            <Button rounded block onPress={() => this.resetPassword()} style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                                <Text>Reset Password</Text>
                            </Button>
                    }

                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    input: {
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15,
    },
    btn_login: {
        width: WIDTH - 35,
        height: 45,
        borderRadius: 25,
        backgroundColor: '#FFA43F',
        justifyContent: 'center',
        marginTop: 20,
        marginLeft: 20,
        shadowColor: 'rgba(232, 34, 32, 2)',
        shadowOpacity: 0.8,
        elevation: 6,
        shadowRadius: 15,
        shadowOffset: {
            width: 1,
            height: 13
        },
    },
    login: {
        fontSize: 16,
        color: 'rgba(255,255,255,0.7)',
        textAlign: 'center'
    },
    errorMessage: {
        marginTop: 15,
        marginLeft: 15,
        fontSize: 15,
        color: 'red',
        alignSelf: 'center',
    },
})