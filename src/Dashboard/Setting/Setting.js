import React from "react";
import { Header, Left, Button, Icon, Body, Container, Text, Card, CardItem, Title, Right } from "native-base";

export default class Setting extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Container>
                <Header style={{ paddingTop: 25, height: 80, backgroundColor: '#5CA2FF' }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Setting</Title>
                    </Body>
                    <Right></Right>
                </Header>
                <Card style={{ borderColor: ('#000000') }} >
                    <CardItem header button onPress={() => this.props.navigation.navigate('Password')}>
                        <Text>Edit Password</Text>
                        <Icon name= "ios-arrow-forward" />
                    </CardItem>
                </Card>
            </Container>
        );
    }
}