import Setting from "./Setting";
import Password from "./Password";
import { createStackNavigator } from "react-navigation";

const SettingStack = createStackNavigator({
    Setting: {
        screen: Setting,
        navigationOptions: {
            header: null
        }
    },
    Password: {
        screen: Password,
        navigationOptions: {
            header: null
        }
    }
})

export default SettingStack;