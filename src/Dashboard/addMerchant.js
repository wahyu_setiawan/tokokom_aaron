import React from "react";
import { Container, Header, Content, Input, Body, Text, Button, Icon, Item, Left, Right, Title, Label } from "native-base";
import { View, StyleSheet } from "react-native";
import * as actions from "../actions";
import { connect } from "react-redux";

class addMerchant extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            image: '',
            error: '',
            namaToko: this.props.navigation.state.params == undefined ? '' : this.props.navigation.state.params.namaToko,
            phone: this.props.navigation.state.params == undefined ? '' : this.props.navigation.state.params.phone,
            alamat: this.props.navigation.state.params == undefined ? '' : this.props.navigation.state.params.alamat,
            describe: this.props.navigation.state.params == undefined ? '' : this.props.navigation.state.params.describe,
        }
    }

    //------------> This Function is used for adding data merchant in addmerchant <---------------------
    addData() {
        if (this.state.namaToko == '' || this.state.phone == '' || this.state.alamat == '' || this.state.describe == '' || this.props.navigation.state.params == undefined) {
            this.setState({ error: 'All field must be filled' })
        } else {
            this.setState({ error: '' })
            var data = {
                namaToko: this.state.namaToko,
                phone: this.state.phone,
                alamat: this.state.alamat,
                describe: this.state.describe
            }
            const { namaToko, phone, alamat, describe } = data;

            //-------------> createNewStore is the result of importing from Redux Action, I'll explain later, <------------------------
            this.props.createNewStore({ namaToko, alamat: this.props.navigation.state.params.alamat, phone, describe, latitude: this.props.navigation.state.params.latitude, longitude: this.props.navigation.state.params.longitude });
            // --------------------> The purpose of this methode is navigate your page one to page two <---------------------
            this.props.navigation.navigate('ViewMerchant');
        }
    }

    render() {
        return (
            <Container>

                {/* Component for header */}
                <Header style={{ paddingTop: 25, height: 80 }}>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name='md-arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{this.props.navigation.state.params == undefined ? 'Add Store' : 'Edit Store'}</Title>
                    </Body>
                    <Right></Right>
                </Header>


                {/* Component for adding value from input Text */}
                <Content>
                    <Item stackedLabel style={styles.input}>
                        <Label style={{ fontWeight: "700" }}>Store Name</Label>
                        <Input placeholder="Alfamart" keyboardType="default" value={this.state.namaToko}
                            onChangeText={(value) => this.setState({ namaToko: value })} />
                    </Item>
                    <Item stackedLabel style={styles.input}>
                        <Label style={{ fontWeight: "700" }}>Phone Number</Label>
                        <Input placeholder="(+62) 812345" keyboardType='phone-pad' value={this.state.phone}
                            onChangeText={(value) => this.setState({ phone: value })} />
                    </Item>
                    <Item stackedLabel style={styles.input}>
                        <Label style={{ fontWeight: "700" }}>Address</Label>
                        <Input placeholder="Google Mountain view" multiline={true} keyboardType="default" value={this.state.alamat}
                            onChangeText={(value) => this.setState({ alamat: value })} />
                    </Item>
                    <Item stackedLabel style={styles.input}>
                        <Label style={{ fontWeight: "700" }}>Description</Label>
                        <Input placeholder="the best store for you" keyboardType="default" value={this.state.describe}
                            onChangeText={(value) => this.setState({ describe: value })} />
                    </Item>


                    {/* The purpose this validation is if we not entered maps, this section doesn't bring up anything, but if we already entered it, your address will appear */}
                    {
                        this.props.navigation.state.params == undefined ?
                            null
                            :
                            <View style={{ marginTop: 12, marginLeft: 15 }}>
                                <Text style={{ textAlign: 'center' }}>
                                    Your Store Address:
                               </Text>
                                <Text style={{ fontWeight: '700', textAlign: 'center' }}>
                                    {this.props.navigation.state.params.alamat}
                                </Text>
                            </View>
                    }

                    {/* Component for Button adding maps */}
                    <Button rounded bordered block style={styles.btn_map} onPress={() => this.props.navigation.navigate('AddingMap')}>
                        <Icon name="ios-pin" />
                        <Text>{this.state.latitude == 0 && this.state.longitude == 0 ? 'Add Location' : 'Change Location'}</Text>
                    </Button>
                    <Button rounded block onPress={() => this.addData()} style={{ marginTop: 20, marginLeft: 15, marginRight: 15 }}>
                        <Text>Save Store</Text>
                    </Button>
                    {
                        this.state.error != '' ?
                            <Text style={{ textAlign: 'center', marginLeft: 15, color: 'red', marginTop: 10 }}>{this.state.error}</Text>
                            :
                            null
                    }
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    btn_save: {
        marginBottom: 15,
        marginRight: 20,
        backgroundColor: '#5CA2FF'
    },
    btn_reset: {
        marginLeft: 20,
        backgroundColor: '#FF0000'
    },
    button: {
        flexDirection: 'row',
        marginTop: 25,
        alignSelf: 'center'
    },
    input: {
        marginTop: 12,
        marginLeft: 15,
        marginRight: 15
    },
    btn_map: {
        marginTop: 12,
        marginLeft: 15,
        marginRight: 15
    }
});

const mapStateToProps = state => {
    const { namaToko, alamat, phone, describe } = state;
    return { namaToko, alamat, phone, describe };

};

export default connect(mapStateToProps, actions)(addMerchant);