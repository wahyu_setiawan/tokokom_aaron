import React from 'react';
import MapView, { Callout } from 'react-native-maps';
import { TouchableOpacity } from 'react-native';
import Ripple from "react-native-material-ripple";
import * as firebase from "firebase";
import { Permissions, Location } from "expo";
// import geolib from 'geolib';
import {
    StyleSheet,
    View,
    Dimensions,
    ScrollView,
    Image
} from 'react-native';
import {
    Container,
    Content,
    Footer,
    FooterTab,
    Button,
    Card,
    CardItem,
    Spinner,
    Text,
    Left,
    Body,
    Right,
    Thumbnail,
    Icon
} from 'native-base';

class AddingMap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            loading: true,
            marker: {

            },
            coords: {},
            search: false,
            streetName: '',
            region: [],
            alamat: ''
        };
    }


    //---------------> component with functions that must be run first <------------------
    componentWillMount() {
        this.getLocation();
        this.getLocationAsync();
    }


    //-----------------> Getting permission for access your location from phone <-----------------------
    getLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
    }

    //--------------> Getting google maps API using fetch metode by region, and then changed to response to be converted to json file <--------------------
    async getMap() {
        if (this.state.region != null) {
            await fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.state.region.latitude + ',' + this.state.region.longitude + '&key=AIzaSyAG4vEyGytmSU8B5YR5cw6e0axEN58wQv0#')
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setNewParams(responseJson.results[0].formatted_address)
                })
                .catch((error) => {
                    console.error(error);
                }).done();

        }
    }


    //-----------------> Get value for alamat, because in the function there is a response and it is changed to json file, we cannot create a state in it, so we need one more function to retrieve the value from the response <---------
    setNewParams(value) {
        var dataMap = {
            latitude: this.state.region.latitude,
            longitude: this.state.region.longitude,
            alamat: value
        };
        this.props.navigation.navigate('AddMerchant', dataMap);
    }


    //---------------> Function for get location by coords from google maps API <--------------------
    async getLocation() {
        await navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    coords: position.coords,
                    error: null
                });
                this.getStore();
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 0 },
        );
    }


    //--------------> Getting google maps API using fetch metode , and then changed to response to be converted to json file <--------------------
    async getStore() {
        if (this.state.coords != null) {
            await fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.state.coords.latitude + ',' + this.state.coords.longitude + '&key=AIzaSyAG4vEyGytmSU8B5YR5cw6e0axEN58wQv0#')
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        streetName: responseJson.results[0].formatted_address,
                        latitude: responseJson.results[0].geometry.location.lat,
                        longitude: responseJson.results[0].geometry.location.lng,
                        loading: false
                    });
                })
                .catch((error) => {
                    console.error(error);
                }).done();
        }
    }


    //---------------------> this function is inherited from react native, the meaning of the region is a red pin that we can move, <------------------
    onRegionChange(region) {
    }
    onRegionChangeComplete(region) {
        this.setState({ region: region })
    }


    render() {

        const { width, height } = Dimensions.get('window');
        const ratio = width / height;

        //------------> If loading true, then spinner will display, but if data already obtained then maps will display <-----------------------------
        if (this.state.loading) {
            return (
                <Spinner color='red' style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5, flex: 2, }} />
            );
        } else {
            return (
                <Container>
                    <Content scrollEnabled={false}>
                        {this.state.error ? <Text>Error: {this.state.error}</Text> : null}
                        <View style={{
                            width, height
                        }}>



                            {/* Component for back again to current page */}
                            <TouchableOpacity style={{ zIndex: 999, position: 'absolute', top: 40, left: 10, backgroundColor: '#FFFFFF', borderRadius: 50, width: 50, height: 50, shadowOffset: { width: 2, height: 2 }, shadowColor: 'black', shadowOpacity: 0.2 }} onPress={() => this.props.navigation.goBack()}>
                                <Icon name="md-arrow-back" style={{ color: '#000000', position: 'relative', top: 10, left: 15 }} />
                            </TouchableOpacity>
                            {this.state.loading ? (
                                <Spinner color='red' />
                            ) : (
                                    //----------------> This for adding store, and getting latitude longitude coordinate from red pin <-----------------------
                                    this.state.coords != null ?
                                        <MapView
                                            style={styles.map}
                                            initialRegion={{
                                                latitude: this.state.coords.latitude,
                                                longitude: this.state.coords.longitude,
                                                latitudeDelta: 0.03 * ratio,
                                                longitudeDelta: 0.03 * ratio
                                            }}
                                            onRegionChange={this.onRegionChange}
                                            onRegionChangeComplete={(region) => { this.onRegionChangeComplete(region) }}
                                        >
                                        </MapView>
                                        :
                                        null
                                )
                            }
                            <View style={styles.calloutView}>
                                <Text>{this.state.alamat}</Text>
                                <Icon name='ios-pin' style={{ fontSize: 46, color: 'red' }} />
                            </View>
                        </View>
                    </Content>


                    {/* Component for get all data from red pin and  will be withdrawn data to adding a merchant page */}
                    <Footer style={styles.btn_pickMap}>
                        <Button transparent onPress={() => this.getMap()}>
                            <Text style={styles.txt_pickMap}>Pin Location</Text>
                        </Button>
                    </Footer>
                </Container >
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'center',
        alignItems: 'center',
    },
    map: {
        marginTop: 1.5,
        ...StyleSheet.absoluteFillObject,
    },
    radius: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        overflow: 'hidden',
        backgroundColor: 'rgba(0,122,255,0.1)',
        borderWidth: 1,
        borderColor: 'rgba(0,112,255,0.3)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    marker: {
        height: 20,
        width: 20,
        borderRadius: 20 / 2,
        overflow: 'hidden',
        backgroundColor: '#007AFF',
        borderWidth: 1,
        borderColor: 'white'
    },
    calloutView: {
        // position: 'absolute',
        justifyContent: "center",
        alignItems: 'center',
        //alignItems: "center",
        // marginLeft: "50%",
        // marginRight: "50%",
        marginTop: "85%",
        //marginBottom:50
    },
    btn_pickMap: {
        backgroundColor: '#5CA2FF'
    },
    txt_pickMap: {
        alignSelf: 'center',
        alignItems: 'center',
        marginTop: 12,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffff'
    }
});

export default AddingMap;
