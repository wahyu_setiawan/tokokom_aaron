import React from "react";
import { Container, Button, Icon, Text } from "native-base";
import { TouchableOpacity, StyleSheet, Image, View, Dimensions, RefreshControl } from "react-native";
import ImageProfil from "../../assets/man.png";
import { connect } from "react-redux";
import { loadStore } from "../actions";
import * as firebase from "firebase";

class merchantAuth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            users: null,
            isLoading: true
        }
    }

    componentWillMount() {
        this.getIdentity();
    }

    getIdentity() {
        const { currentUser } = firebase.auth();
        firebase.database().ref(`/users/${currentUser.uid}/toko`)
            .on("value", (data) => {
                // console.log(data.child('namaToko').val());
                if (data) {
                    this.setState({
                        users: data.val(),
                        isLoading: false
                    })
                }
            })
    }
}