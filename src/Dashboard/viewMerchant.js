import React from "react";
import { Container, Button, Icon, Text, Footer, FooterTab, Content } from "native-base";
import { TouchableOpacity, StyleSheet, Image, View, Dimensions, RefreshControl } from "react-native";
import ImageProfil from "../../assets/man.png";
import { connect } from "react-redux";
import { loadStore } from "../actions";
import * as firebase from "firebase";
import { ConfirmDialog } from "react-native-simple-dialogs";
import Icon2 from "react-native-vector-icons/MaterialIcons";
import * as  Animatable from "react-native-animatable";



class viewMerchant extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            deviceHeight: Dimensions.get('window').height,
            deviceWidth: Dimensions.get('window').width,
            users: null,
            isLoading: true,
            isModalVisible: false,
            refreshing: false
        }
    }

    //------------------------> Function for running first time component if we click or direct to this page <----------------------------
    componentWillMount() {
        this.getIdentity();
    }


    //-------------------> Function for simple dialog authentication, this function will be showing if you press button sign out <-------------------- 
    openConfirm = (show) => {
        this.setState({ showConfirm: show });
    }

    optionYes = async () => {
        this.openConfirm(false);
        try {
            await firebase.auth().signOut();
        } catch (e) {
            console.log(e);
        }
        setTimeout(() => {
            alert('You Log out Now !')
        }, 300);

        this.props.navigation.navigate('Main');
    }

    optionNo = () => {
        this.openConfirm(false);
        setTimeout(() => {
            this.props.navigation.navigate('ViewMerchant')
        }, 300);
    }
    //------------------------------------------------------------



    //-------------------------> Function for get data from firebase, in this case, we just need data toko from firebase <------------------
    getIdentity() {
        const { currentUser } = firebase.auth();
        firebase.database().ref(`/users/${currentUser.uid}/toko`)
            .on("value", (data) => {
                if (data) {
                    this.setState({
                        users: data.val(),
                        isLoading: false
                    })
                }
            })
    }



    render() {
        //----------------> In this section, we have 2 condition with this.state.isLoading, if loading true app displaying loading, and else app return users <------------------
        if (this.state.isLoading) {
            return (
                <Container style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <Text>Loading</Text>
                </Container>
            )
        }
        else {
            return (
                // ---------> Section for validation, if user first time to register, app will return page you dont have store, but if the user already has a store app displaying data from firebase <--------------
                !this.state.users ?
                    <Container>
                        <View style={{
                            flex: 1,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}>
                            <Text style={{ marginBottom: 15, fontWeight: "700" }}>You don't have any store</Text>
                            <Button onPress={() => this.props.navigation.navigate('AddMerchant')}
                                style={{ alignSelf: 'center' }}
                                rounded outlined bordered>
                                <Icon name="md-home" />
                                <Text>Create Store</Text>
                            </Button>
                        </View>
                    </Container>
                    :
                    <Container>
                        <Content>
                            <View style={styles.ImgProfil}>
                                <TouchableOpacity>
                                    <Image source={ImageProfil} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignItems: 'center', marginTop: 10, marginBottom: 20 }}>
                                <Text style={{ fontWeight: '700', fontSize: 24, marginBottom: 5 }}>{this.state.users.namaToko}</Text>
                                <Text>{this.state.users.describe}</Text>
                            </View>
                            <View style={{ marginLeft: 20, marginRight: 20, alignContent: 'center' }}>
                                <View style={{ flexDirection: 'row', marginBottom: 10, }}>
                                    <Icon style={{ marginRight: 5 }} name="md-call" />
                                    <Text style={{ marginLeft: 5 }}>{this.state.users.phone}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Icon style={{ marginRight: 5, marginTop: 10 }} name="md-pin" />
                                    <Text style={{ margin: 10 }}>{this.state.users.alamat}</Text>
                                </View>
                            </View>

                            <Button rounded style={{ alignSelf: 'center', marginTop: 40 }} onPress={() => this.props.navigation.navigate('AddMerchant', this.state.users)}>
                                <Text>Edit My Store</Text>
                                <Icon name="ios-arrow-forward" />
                            </Button>


                            {/*Component for validation, if you want to sign out from dashboard merchant  */}
                            <ConfirmDialog
                                title="Confirm Dialog"
                                message="Are you sure log out ?"
                                onTouchOutside={() => this.openConfirm(false)}
                                visible={this.state.showConfirm}
                                negativeButton={
                                    {
                                        title: "NO",
                                        onPress: this.optionNo,
                                        titleStyle: {
                                            color: "blue",
                                            colorDisabled: "aqua",
                                        },
                                        style: {
                                            backgroundColor: "transparent",
                                            backgroundColorDisabled: "transparent",
                                        },
                                    }
                                }
                                positiveButton={
                                    {
                                        title: "YES",
                                        onPress: this.optionYes,
                                    }
                                }
                            />
                        </Content>


                        {/* This component just footer tabs */}
                        <Animatable.View animation="zoomIn" duration={1200}>
                            <Footer >
                                <FooterTab>
                                    <Button vertical onPress={() => this.props.navigation.navigate('ProductList')}>
                                        <Icon name="md-cube" />
                                        <Text>Product</Text>
                                    </Button>
                                    <Button vertical onPress={() => this.openConfirm(true)}>
                                        <Icon name="ios-log-out" />
                                        <Text>Sign Out</Text>
                                    </Button>
                                </FooterTab>
                            </Footer>
                        </Animatable.View>

                    </Container>
            )
        }
    }
}

const styles = StyleSheet.create({
    ImgProfil: {
        alignSelf: 'center',
        width: 150,
        height: 150,
        marginTop: 50,
        marginLeft: 10
    },
    btn_setting: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        height: 60,
        backgroundColor: '#261B07',
        borderRadius: 100,
        marginRight: 17,
        marginBottom: 20
    },
    btn_addProduct: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        borderRadius: 100,
        height: 60,
        backgroundColor: '#261B07',
        marginBottom: 20,
    },
    btn_logout: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        borderRadius: 100,
        height: 60,
        backgroundColor: '#261B07',
        marginLeft: 17,
        marginBottom: 20
    },
    namaToko: {
        fontSize: 20,
        fontWeight: 'bold',

    }
});

export default connect(null, { loadStore })(viewMerchant);