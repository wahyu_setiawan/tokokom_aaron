import React from "react";
import {
    Header, Body, Container, Text, Right, Button,
    Icon, Form, Item, Input, Label, View, Card, CardItem, Left, Thumbnail, Content, Spinner, Title
} from "native-base";
import { TextInput, ScrollView, RefreshControl, Image, Animated, Easing, Dimensions, Slider, Modal } from "react-native";
import * as firebase from "firebase";
import geolib from "geolib";
import IconMerchant from "../../assets/grocery.png";
import HeaderIcon from "../../assets/newlogo.png";
import { Permissions, WebBrowser, Constants, Linking, } from "expo";
// import { DialogComponent } from "react-native-dialog-component";


export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: [],
            latitude: '',
            longitude: '',
            region: {
                latitude: 37.78825,
                longitude: -122.4324,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
            },
            users: [],
            filteredData: '',
            isModalVisible: false,
            isLoading: true,
            coords: {},
            refreshing: false,
            InitialLinkingUri: '',
            toko: [],
            progress: new Animated.Value(0),
            isVisible: true,
            distance: 10,
            keywordSearch: '',
            distanceSearch: 0
        }
    }


    //---------> Function for getting URL app from expo <--------------------
    _addLinkingListener = () => {
        Linking.addEventListener('url', this._handleRedirect);
    };

    _removeLinkingListener = () => {
        Linking.removeEventListener('url', this._handleRedirect);
    };

    _handleRedirect = event => {
        WebBrowser.dismissBrowser();
        let data = Linking.parse(event.url);
        this.setState({ redirectData: data });
    };

    _openWebBrowserAsync = async () => {
        try {
            this._addLinkingListener();
            let result = await WebBrowser.openBrowserAsync(
                // We add `?` at the end of the URL since the test backend that is used
                // just appends `authToken=<token>` to the URL provided.
                `https://backend-xxswjknyfi.now.sh/?linkingUri=${Linking.makeUrl('/?')}`
            );
            this._removeLinkingListener();
            this.setState({ result });
        } catch (error) {
            alert(error);
            console.log(error);
        }
    };
    // ------------------------------------------------------------------------------------




    //------------------> Function used to read the first component / function that must be run <-------------------
    async componentWillMount() {
        firebase.initializeApp({
            apiKey: "AIzaSyDQ2Yoe7LobogKfWhE4tEVmc57PzyCnwmY",
            authDomain: "tokokom-c4f36.firebaseapp.com",
            databaseURL: "https://tokokom-c4f36.firebaseio.com",
            projectId: "tokokom-c4f36",
            storageBucket: "tokokom-c4f36.appspot.com",
            messagingSenderId: "862669322894"
        });
        this.getUser();
        this.getLocation();

        const InitialLinkingUri = await Linking.getInitialURL();
        this.setState({ InitialLinkingUri });

    }
    // ---------------------------------------------------------------------------------------




    //-------------------------> Function for pull to refresh on cardView at below current User location <--------------------------
    onRequest() {
        this.setState({ refreshing: true });
        this.getLocation().then(() => {
            this.setState({ refreshing: false })
        });
    }
    // ----------------------------------------------------------------------------------------



    //-------------------------> Function for btn refresh in below section, if you press button you'll see your current user <---------------------------- 
    btn_Refresh() {
        this.getUser();
        this.getLocation()
    }
    // ---------------------------------------------------------------------------------------------


    //------------------------> Function for Getting data from firebase, <--------------------------------------- 
    getUser() {
        var rootRef = firebase.database().ref('users');
        rootRef.on("value", (response) => {
            this.state.users = [];
            response.forEach((data) => {
                this.state.users.push(data)
            })
            this.setState({ isLoading: false })
        })
    }
    // ---------------------------------------------------------------------------------------------



    // --------------------> Function for getting current location from google API <--------------------------------
    async getLocation() {
        await navigator.geolocation.watchPosition(
            (position) => {
                this.setState({
                    coords: position.coords,
                    error: null
                });
                this.getStore();
            },
            (error) => console.log(error),
            { enableHighAccuracy: true, timeout: 5000, maximumAge: 0 },
        );
    }

    getStore() {
        if (this.state.coords != null) {
            fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.state.coords.latitude + ',' + this.state.coords.longitude + '&key=AIzaSyAG4vEyGytmSU8B5YR5cw6e0axEN58wQv0#')
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        streetName: responseJson.results[0].formatted_address,
                        latitude: responseJson.results[0].geometry.location.lat,
                        longitude: responseJson.results[0].geometry.location.lng,
                        loading: false
                    });
                })
                .catch((error) => {
                    console.error(error);
                }).done();
        }
    }
    // --------------------------------------------------------------------------------------------------


    // ------------------------> Function for filtered data by keyword and distance <--------------------------------------
    filterSearch() {
        var previousState = this.state.users
        if (this.state.keywordSearch == '') {
            this.getUser();
            this.setState({ isModalVisible: false })
        }
        else if (this.state.keywordSearch != '') {
            this.setState({
                users: previousState.filter(data => {
                    if (data.child('toko/namaToko').val().toLowerCase().indexOf(this.state.keywordSearch.toLowerCase()) > -1) {
                        return data
                    }
                }),
                isModalVisible: false
            });
        }
    }
    // ---------------------------------------------------------------------------------------------------------



    // --------------------------------> Function for reset value in search page, and back again in our current location <-------------------------------------
    resetSearch() {
        this.getUser();
        this.setState({ isModalVisible: false, keywordSearch: '', distance: 10 })
    }
    // ---------------------------------------------------------------------------------------------------------



    render() {
        return (
            <Container >
                {/* Component for header app in first time we run app, there is Logo, button search and then button for login*/}
                <Header style={{ paddingTop: 25, height: 80, backgroundColor: '#5CA2FF' }}>
                    <Left>
                        <Image source={HeaderIcon} style={{ width: 120, height: 120 }} resizeMode="contain" />
                    </Left>
                    <Body>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.setState({ isModalVisible: true })}>
                            <Icon name='md-search' style={{ color: 'white' }} />
                        </Button>
                        <Button transparent onPress={() => this.props.navigation.navigate('Login')}>
                            <Icon name='md-person' style={{ color: 'white' }} />
                        </Button>
                    </Right>
                </Header>

                {/* Button in below sectin for refresh data and current location */}
                <Button icon rounded onPress={() => this.btn_Refresh()} style={{ position: 'absolute', bottom: 15, right: 15, width: 50, height: 50, zIndex: 99 }}>
                    <Icon name="md-refresh" style={{ position: 'relative', left: 2 }} />
                </Button>

                {/* Component for displaying current location with the name */}
                <View style={{ padding: 15, flexDirection: 'row', borderBottomColor: '#DDDDDD', borderBottomWidth: 1 }}>
                    <View style={{ alignSelf: 'center', marginRight: 10 }}>
                        <Icon name="ios-pin" />
                    </View>
                    <View style={{ alignSelf: 'flex-end' }}>
                        <Text>Your Current Location</Text>
                        <Text style={{ fontWeight: 'bold', paddingRight: 13 }}>{this.state.streetName}</Text>
                    </View>
                </View>



                {/* Component for search page, but actually that's just modal  */}
                <Modal
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => this.resetSearch()}
                    visible={this.state.isModalVisible}
                    onDismiss={() => this.setState({ isModalVisible: false })}
                >
                    <Header style={{ paddingTop: 25, height: 70, }}>
                        <Left>
                            <Button transparent onPress={() => this.setState({ isModalVisible: false })}>
                                <Icon name='md-arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Search</Title>
                        </Body>
                        <Right />
                    </Header>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 40
                    }}>Enter Your Keyword</Text>
                    <TextInput style={{
                        height: 40, marginLeft: 30, marginRight: 30, marginTop: 20,
                        marginBottom: 10, borderBottomWidth: 1
                    }} placeHolder="Name Store" value={this.state.keywordSearch} onChangeText={(value) => this.setState({ keywordSearch: value })} />
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 10
                    }}>Distance </Text>
                    <Slider
                        step={1}
                        maximumValue={20}
                        thumbTintColor='rgb(252, 228, 149)'
                        maximumTrackTintColor='#d3d3d3'
                        value={this.state.distance}
                        onValueChange={(val) => this.setState({ distance: val })}
                        minimumTrackTintColor='rgb(252, 228, 149)'
                        style={{ paddingLeft: 10, paddingRight: 10 }}
                    />
                    <Text style={{
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginTop: 10,
                        marginBottom: 10
                    }}>{this.state.distance} km</Text>
                    <Button rounded bordered block style={{ marginLeft: 15, marginRight: 15, marginTop: 15 }} onPress={() => this.filterSearch()}>
                        <Text>Search</Text>
                    </Button>
                    <Button rounded bordered block style={{ marginLeft: 15, marginRight: 15, marginTop: 15 }} onPress={() => this.resetSearch()}>
                        <Text>Reset</Text>
                    </Button>
                </Modal>


                {/* Component for scrollview with data from get User */}
                <ScrollView refreshControl={<RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.onRequest.bind(this)}
                />}>

                    {/* this is condition if we get latitude and longitude from google API, component will display data, else spinner must be run */}
                    {this.state.isLoading ?
                        <Spinner color="#5CA2FF" style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5, flex: 2, }} />
                        :
                        <View>
                            {this.state.users.map((data, i) => {


                                // the method used to calculate the distanc from latitude, longitude stores to the current user now 
                                if (data.child('toko/latitude').val()) {
                                    var jarak = geolib.getDistance(
                                        { latitude: this.state.latitude, longitude: this.state.longitude },
                                        { latitude: data.child('toko/latitude').val(), longitude: data.child('toko/longitude').val() }
                                    )
                                    var jarak_convert = geolib.convertUnit('km', jarak, 2)
                                    if (jarak_convert <= this.state.distance) {
                                        this.state.toko.push(data)
                                    }
                                } else {
                                    var jarak = 0;
                                    var jarak_convert = 0;
                                }

                                if (jarak_convert <= this.state.distance) {
                                    return (
                                        <Card key={i}>

                                            {/*  The use of throwing data from the page in to another page by adding {data} behind to bring the data to the destination page*/}
                                            <CardItem button onPress={() => this.props.navigation.navigate('DetailView', { data })}>
                                                <Left>
                                                    <Thumbnail source={IconMerchant} />
                                                    <Body>
                                                        <Text>{data.child('toko/namaToko').val()}</Text>
                                                        <Text>{data.child('toko/alamat').val()}</Text>
                                                        {/* <Text>{data.child('toko/phone').val()}</Text> */}
                                                        <Text note>{jarak_convert} km</Text>
                                                    </Body>
                                                </Left>
                                            </CardItem>
                                        </Card>
                                    )
                                }
                            })
                            }

                            {/* Condition if we dont heve merchant from specified distance, then there will be no store nearby */}
                            {
                                this.state.toko.length == 0 ?
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                                        <Icon name="md-sad" style={{ color: '#CCCCCC' }} />
                                        <Text style={{ color: '#CCCCCC' }}>No Nearby Store</Text>
                                    </View>
                                    :
                                    null
                            }
                        </View>
                    }
                </ScrollView>
            </Container>
        )
    }
}

