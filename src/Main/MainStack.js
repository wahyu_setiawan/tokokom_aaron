import { createStackNavigator } from 'react-navigation';
import MainPage from './Main';
import Login from '../Auth/login';
import Register from "../Auth/Register";
import Dashboard from "../Dashboard/DashboardStack";
import storeItem from "../component/storeItem";
import DetailView from "../Dashboard/Product/DetailView"
import ResetPassword from "../Dashboard/Setting/Password";
import SuccessReset from "../Auth/SuccessReset";
import RegisterAuth from "../Auth/RegisterAuth";

const MainStack = createStackNavigator({
    Main: {
        screen: MainPage,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },
    Register: {
        screen: Register,
        navigationOptions: {
            header: null
        }
    },
    Dashboard: {
        screen: Dashboard,
        navigationOptions: {
            header: null
        }
    },
    storeItem: {
        screen: storeItem,
        navigationOptions: {
            header: null
        }
    },
    DetailView: {
        screen: DetailView,
        navigationOptions: {
            header: null
        }
    },
    ResetPassword: {
        screen: ResetPassword,
        navigationOptions: {
            header: null
        }
    },
    SuccessReset: {
        screen: SuccessReset,
        navigationOptions: {
            header: null
        }
    },
    RegisterAuth: {
        screen: RegisterAuth,
        navigationOptions: {
            header: null
        }
    }
})

export default MainStack;
