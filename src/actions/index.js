import * as firebase from "firebase";

export const selectStore = (tokoId) => {
    return {
        type: "SELECT_STORE",
        payload: tokoId
    }
}

export const selectBarang = (barangId) => {
    return {
        type: "SELECT_BARANG",
        payload: barangId
    }
}

export const noneSelected = () => {
    return {
        type: 'NONE_SELECTED',
    };
};

export const formUpdate = ({ prop, value }) => {
    return {
        type: 'FORM_UPDATE',
        payload: { prop, value },
    };
};

export const formUpdateBarang = ({ prop, value }) => {
    return {
        type: 'FORM_UPDATE_BARANG',
        payload: { prop, value },
    };
};

export const createNewStore = ({ namaToko, alamat, phone, describe, latitude, longitude }) => {
    const { currentUser } = firebase.auth();
    return (dispatch) => {
        var dataToko = { namaToko, alamat, phone, describe, latitude, longitude };
        var toko = { toko: dataToko };
        firebase.database().ref('/users/' + currentUser.uid)
            .update(toko)
            .then(() => {
                dispatch({ type: 'NEW_CONTACT' });
            });
    };
};

export const createNewBarang = ({ namaBarang, hargaBarang, beratBarang, gambar }) => {
    const { currentUser } = firebase.auth();
    var uid = currentUser.uid;
    return (dispatch) => {
        var dataBarang = { namaBarang, hargaBarang, beratBarang, gambar };
        var item = { item: dataBarang };
        firebase.database().ref('users/' + uid + '/barang')
            .push(dataBarang)
            .then(() => {
                dispatch({ type: 'NEW_GOODS' })
            });
    };
};

export const loadStore = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/toko`)
            .on('value', snapshot => {
                dispatch({ type: 'INITIAL_FETCH', payload: snapshot.val() });
            });
    };
};

const logOut = () => {
    firebase.auth().signOut().then(function () {
        // console.log('success')
    },
        function (error) {
            // console.log('error')
        })
}

export const loadProduct = () => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/toko`).child('barang')
            .on('value', snapshot => {
                dispatch({ type: 'INITIAL_BARANG', payload: snapshot.val() });
            });
    };
};


export const deleteProduct = (uid) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/barang/${uid}`)
            .remove()
            .then(() => {
                dispatch({ type: 'DELETE_PRODUCT' });
            });
    };
};

export const updateContact = (personSelected) => {
    return {
        type: 'UPDATE_CONTACT',
        payload: personSelected,
    };
};

export const saveContact = ({ first_name, last_name, phone, email, company, project, notes, uid }) => {
    const { currentUser } = firebase.auth();

    return (dispatch) => {
        firebase.database().ref(`/users/${currentUser.uid}/people/${uid}`)
            .set({ first_name, last_name, phone, email, company, project, notes, uid })
            .then(() => {
                dispatch({ type: 'SAVE_CONTACT' });
            });
    };
}