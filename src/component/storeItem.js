import React, { Component } from "react";
import { Text, Image, View, StyleSheet, TouchableWithoutFeedback } from "react-native";
import { connect } from "react-redux";
// import * as actions from "../actions";
import { Icon, Card } from "native-base";
import { getTheme } from "react-native-material-kit";
import * as actions from "../actions";


const theme = getTheme();

const storeItem = (props) => {
    return (
        // <Card>
            <View style={[theme.cardStyle, styles.card]}>
                <Image
                    source={{ uri: '' }}
                    style={[theme.cardImageStyle, styles.icon]}
                />
                <Text style={[theme.cardTitleStyle, styles.title]}>{props.toko.namaBarang} </Text>
                {/* <Text style={[theme.cardActionStyle, styles.action]}>{props.toko.hargaBarang}</Text> */}
            </View>
        // </Card>
    );
};

// console.log(props.toko.namaBarang);
const styles = StyleSheet.create({
    card: {
        marginTop: 20,
    },
    
    title: {
        top: 20,
        left: 80,
        fontSize: 24,
    },
    image: {
        height: 100,
    },
    action: {
        backgroundColor: 'black',
        color: 'white',
    },
    icon: {
        position: 'absolute',
        top: 15,
        left: 0,
        color: 'white',
        backgroundColor: 'rgba(255,255,255,0)',
    },
});

export default connect(null, actions)(storeItem);