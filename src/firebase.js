import React from "react";
import * as firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyDQ2Yoe7LobogKfWhE4tEVmc57PzyCnwmY",
    authDomain: "tokokom-c4f36.firebaseapp.com",
    databaseURL: "https://tokokom-c4f36.firebaseio.com",
    projectId: "tokokom-c4f36",
    storageBucket: "tokokom-c4f36.appspot.com",
    messagingSenderId: "862669322894"
};

firebase.initializeApp(firebaseConfig);