// import store from "./store.json";

const initialState = {
    toko: [],
    barang: [],
    detailView: false,
    selectBarang: null,
    namaToko: "",
    alamat: "",
    phone: "",
    namaBarang: "",
    hargaBarang: "",
    beratBarang: "",
    toUpdate: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'INITIAL_FETCH':
            return {
                ...state,
                toko: action.payload,
            }

        case 'INITIAL_BARANG':
            return {
                ...state,
                barang: action.payload,
            };

        case 'SELECT_BARANG':
            return {
                ...state,
                detailView: true,
                selectBarang: action.payload
            };

        case 'NONE_SELECTED':
            return{
                ...state,
                detailView: false,
                selectBarang: null,
            }

        case 'FORM_UPDATE':
            return {
                ...state,
                [action.payload.prop]: action.payload.value
            };

        case 'FORM_UPDATE_BARANG':
            return {
                ...state,
                [action.payload.prop]: action.payload.value
            };

        case 'NEW_CONTACT':
            return {
                ...state,
                namaToko: '',
                alamat: '',
                phone: '',
                describe: '',
            }

        case 'NEW_GOODS':
            return {
                ...state,
                namaBarang: '',
                hargaBarang: '',
                beratBarang: '',
                gambar: ''
            }

        case 'UPDATE_CONTACT':
            return {
                ...state,
                toUpdate: true,
                namaToko: action.payload.namaToko,
                alamat: action.payload.alamat,
                uid: action.payload.uid,
            }

        case 'SAVE_CONTACT':
            return {
                ...state,
                toUpdate: false,
                detailView: false,
                first_name: '',
                last_name: '',
                phone: '',
                email: '',
                company: '',
                project: '',
                notes: '',
                uid: '',
            };

        case 'DELETE_PRODUCT':
            return {
                ...state,
                detailView: false,
                personSelected: null,
            }
        default:
            return state;

    }
}